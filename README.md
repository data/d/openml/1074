# OpenML dataset: desharnais

https://www.openml.org/d/1074

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, verifiable, refutable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please
follow the acknowledgment guidelines posted on the PROMISE repository
web page http://promise.site.uottawa.ca/SERepository .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Domain: software cost estimation
Donor:  Martin Shepperd <Martin.Shepperd@brunel.ac.uk>
Corrections by: Dan Port <dport@hawaii.edu> with information from Barbara Kitchenham <barbara@cs.keele.ac.uk>

Notes:  4 incomplete projects (projects 38,44,66,75) so often people use the 77 complete cases.
The original source is [1].
Our paper [2] provides a basic overview of the data set.
References:
[1] J. M. Desharnais, "Analyse statistique de la
productivitie des projets informatique a partie de la
technique des point des fonction," University of Montreal,
Masters Thesis, 1989.

[2] M. J. Shepperd and C. Schofield, "Estimating software
project effort using analogies," IEEE Transactions on
Software Engineering, vol. 23, pp. 736-743, 1997.

[3] Dreger, J. Brian. "Function Point Analysis"
Englewood Cliffs, NJ: Prentice Hall, 1989.

Attributes:


Note: Many of the above attributes are numerically coded catagorical variables. Values of -1 indicate missing data,

Sample run (WEKA):
=== Run information ===

Scheme:       weka.classifiers.functions.LinearRegression -S 0 -R 1.0E-8
Relation:     martin.csv
Instances:    81
Attributes:   12
Project
TeamExp
ManagerExp
YearEnd
Length
Effort
Transactions
Entities
PointsNonAdjust
Adjustment
PointsAjust
Langage

Note from dport: the regression model below is problematic in that it treats catagorical variables as continuous values
and it includes the dependent variable Length as an independent variable which is likely a mistake.

Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Linear Regression Model

Effort =
-433.25   * TeamExp +
408.8057 * ManagerExp +
201.2701 * Length +
4.2361 * Transactions +
7.9056 * Entities +
4.4594 * PointsAdjust +
92.1389 * Adjustment +
-1777.4579 * Langage +
-278.0786

Time taken to build model: 0.02 seconds

=== Cross-validation ===
=== Summary ===

Correlation coefficient                  0.7303
Mean absolute error                   2082.1182
Root mean squared error               3007.0504
Relative absolute error                 64.9965 %
Root relative squared error             67.3788 %
Total Number of Instances               81

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1074) of an [OpenML dataset](https://www.openml.org/d/1074). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1074/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1074/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1074/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

